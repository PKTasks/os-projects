// PortMaster.c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/file.h>
#include <sys/stat.h>
#include <semaphore.h>
#include <sys/ipc.h>
#include <sys/types.h>
#include <sys/shm.h>

#include "Structs.h"

int main(int argc, char *argv[]){
	printf("PortMaster: my pid is %d. my parent id is [%d].\n",getpid(), getppid() );

	int shemMemId;
	FILE *fdCharge;
	

	if(argc == 5){
		for (int i = 0; i < argc; i++){
			if(strcmp(argv[i], "-s") == 0){
				shemMemId = atoi(argv[i+1]);
				
				printf("sharedMem id: %d\n",shemMemId );
			}
			if(strcmp(argv[i], "-c") == 0){
				fdCharge = fopen(argv[i+1],"r");
				
			}
		}
	}
	else{
		printf("give me all the args\n");
		exit(-1);
	}



	char *line = NULL;
	size_t bytes = 0;
	char *token;
	char costS[5], costM[5], costL[5];
	int counter = 0;

	while (getline(&line, &bytes, fdCharge) != -1){
		line[strlen(line) - 1] = '\0';

		token = strtok(line, " ");
		while(token != NULL){
			if(counter == 1){
				strcpy(costS,token);
			}
			if(counter == 3){
				strcpy(costM,token);
			}
			if(counter == 5){
				strcpy(costL,token);
			}
			token = strtok(NULL, " ");
			counter++;
		}
	}
	fclose(fdCharge);

	//ataching shared memory
	sharedMem *shmem;
	shmem = (sharedMem *)shmat(shemMemId,(void *)0,0);

	printf("smallCap %d\n",shmem->smallCap );
	printf("midCap %d\n",shmem->midCap );
	printf("largeCap %d\n",shmem->largeCap );
	
}