#define LARGE 5
#define MID 8
#define SM 10

typedef struct{
	char *name;
	double cost;
	char arrivalTime[10];
	char parkType[2];

} ledger;




typedef struct{
	ledger *leg;
} port;



typedef struct{
	int smallCap;
	int midCap;
	int largeCap;
	port *myport;
	sem_t fullPort;
	sem_t largeV;
	sem_t midV;
	sem_t smallV;
} sharedMem;