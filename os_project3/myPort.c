//myport
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/file.h>
#include <sys/stat.h>
#include <semaphore.h>
#include <sys/ipc.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <sys/wait.h>

#include "Structs.h"

int main(int argc, char *argv[]){
	char *configfile;
	if(argc == 3){
		for (int i = 0; i < argc; i++){
			if(strcmp(argv[i], "-l") == 0){
				configfile = malloc(strlen(argv[i+1])*sizeof(char) + 1);
				strcpy(configfile,argv[i+1]);
				// printf("%s\n",configfile );
			}
		}
	}
	else{
		printf("give me all the args\n");
		exit(-1);
	}



	//creating shared memory
	int shemMemId=0, err=0;
	sharedMem *mem;

	/* Make shared memory segment */
	shemMemId = shmget(IPC_PRIVATE,sizeof(sharedMem),0666); 
	if (shemMemId == -1) 
		perror ("Creation");
	else 	
		printf("Allocated Shared Memory with shemMemId: %d\n",(int)shemMemId);

	char shemMemIdStr[10];
	sprintf(shemMemIdStr,"%d",shemMemId);

	/* Attach the segment */
	mem = (sharedMem *) shmat(shemMemId, (void*)0, 0);
	/**/




	//creating semaphores
	int retval = sem_init(&(mem->fullPort),1,0);
	if(retval != 0){
		perror("couldn't initialise semaphore");
		exit(3);
	}

	retval = sem_init(&(mem->largeV),1,0);
	if(retval != 0){
		perror("couldn't initialise semaphore");
		exit(3);
	}

	retval = sem_init(&(mem->midV),1,0);
	if(retval != 0){
		perror("couldn't initialise semaphore");
		exit(3);
	}
	retval = sem_init(&(mem->smallV),1,0);
	if(retval != 0){
		perror("couldn't initialise semaphore");
		exit(3);
	}
	/**/



	//opening configuration file
	FILE *fdConf;
	fdConf = fopen(configfile,"r");
	FILE *fdCharge;
	fdCharge = fopen("charges.txt","w");
	char *line = NULL;
	size_t bytes = 0;
	char *token;
	char type1[5], type2[5], type3[5];
	char type1Cost[10], type2Cost[10], type3Cost[10];
	int counter = 0;
	while (getline(&line, &bytes, fdConf) != -1){
		line[strlen(line) - 1] = '\0';

		token = strtok(line, " ");
		while(token != NULL){
			// printf("%s\n",token );

			if(counter == 1){  //type small
				strcpy(type1Cost,token);
				strcpy(type1,token);
				// printf("%s\n",type1 );
			}
			else if(counter == 3){ //type medium
				strcpy(type2Cost,token);
				strcpy(type2,token);
				
			}
			else if(counter == 5){  //type large
				strcpy(type3Cost,token);
				strcpy(type3,token);
				
			}
			else if(counter == 7){
				mem->smallCap = atoi(token); // initialising small capacity into sem mem
			}
			else if(counter == 9){
				mem->midCap = atoi(token); // initialising medium capacity into sem mem
			}
			else if(counter == 11){
				mem->largeCap = atoi(token); // initialising large capacity into sem mem
			}
			else if(counter == 13){ //cost small
				strcat(type1Cost," ");
				strcat(type1Cost,token);
				strcat(type1Cost,"\n");
				fwrite(type1Cost,strlen(type1Cost),1,fdCharge);
				
			}
			else if(counter == 15){  //cost medium
				strcat(type2Cost," ");
				strcat(type2Cost,token);
				strcat(type2Cost,"\n");
				fwrite(type2Cost,strlen(type2Cost),1,fdCharge);
				
			}
			else if(counter == 17){  //cost large
				strcat(type3Cost," ");
				strcat(type3Cost,token);
				strcat(type3Cost,"\n");
				fwrite(type3Cost,strlen(type3Cost),1,fdCharge);
				
			}
			else if(19){
				
			}
			token = strtok(NULL, " ");
			counter++;
		}
	}
	fclose(fdCharge);
	/**/


	

	//initialising port master
	int pidPortMaster;
	
	pidPortMaster = fork();
	if(pidPortMaster == 0){
		if(execlp("./PortMaster","PortMaster","-c","charges.txt","-s",shemMemIdStr,(char*)NULL) == -1){
			perror("exec");
		};
	}
	/**/


	//initialising monitor
	int pidMonitor;
	
	pidMonitor = fork();
	if(pidMonitor == 0){
		if(execlp("./Monitor","Monitor","-d","b","-t","c","-s",shemMemIdStr,(char*)NULL) == -1){
			perror("exec");
		};
	}
	/**/


	//initialising vessels
	int pidVessel;

	pidVessel = fork();
	if(pidVessel == 0){
		if(execlp("./Vessel","Vessel","-t",type1,"-u","c","-p","v","-s",shemMemIdStr,"a","b",(char*)NULL) == -1){
			perror("exec");
		};
	}
	/**/



	waitpid(pidPortMaster, NULL, 0);
	waitpid(pidMonitor, NULL, 0);


	fclose(fdConf);
}
