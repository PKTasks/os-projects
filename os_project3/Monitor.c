// Monitor.c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/file.h>
#include <sys/stat.h>
#include <semaphore.h>
#include <sys/ipc.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <sys/wait.h>

#include "Structs.h"

int main(int argc, char *argv[]){
	printf("Monitor: my pid is %d. my parent id is [%d].\n",getpid(), getppid() );

	int shemMemId;
	if(argc == 7){
		for (int i = 0; i < argc; i++){
			if(strcmp(argv[i], "-s") == 0){
				shemMemId = atoi(argv[i+1]);
				
				printf("sharedMem id: %d\n",shemMemId );
			}
		}
	}
	else{
		printf("give me all the args\n");
		exit(-1);
	}

	//ataching shared memory
	sharedMem *shmem;
	shmem = (sharedMem *)shmat(shemMemId,(void *)0,0);

	// printf("%d\n",shmem->number );
}