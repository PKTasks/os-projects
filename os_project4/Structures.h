typedef struct treeNode;

typedef struct{
	unsigned int inodeId;
	time_t lastChange;
	long fileSize;
	char **namesArray;
	int namesCount;
	struct inode *inodeP;
} inode;


typedef struct nodeList{  //arxh tou komvou
	struct treeNode *firstNode;
	int numOfNodes;  //prepei na to afxanw kathe fora p kanw insert
} nodeList;


typedef struct{
	char *name;
	char type;
	inode *inodeP;
	nodeList *head;
} treeNode;