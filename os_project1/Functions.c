#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "Graph.h"
#include "Functions.h"
#include "HelpFuncts.h"


void insertNode(char *id, struct nodeList *nodelist, int thereIsOutput, FILE *fdOutput){
	struct node *tempNode;

	tempNode = searchNodeList(id, nodelist);


 
	if(tempNode == NULL){
		tempNode = nodeInit(id);
		tempNode->next = nodelist->firstNode;

		if(nodelist->firstNode != NULL){
			(nodelist->firstNode)->previous = tempNode;///////////////
		}
		
		nodelist->firstNode = tempNode;

		nodelist->numOfNodes = nodelist->numOfNodes + 1;


		printf("Inserted |%s|\n",id );
		

	}
	else{

		printf("Node |%s| Exists;\n",id );
		
		
	}
	

}

void insertEdge(char *senderStr,char *receiverStr, char *weight, struct nodeList *nodelist, int thereIsOutput, FILE *fdOutput){

	
//############################// ftiaxnw tous komvous an den uparxoun
	struct node *tempSender;
	struct node *tempReceiver;


	tempSender = searchNodeList(senderStr, nodelist);

	if(tempSender == NULL){  //apostoleas
		tempSender = nodeInit(senderStr);
		tempSender->next = nodelist->firstNode;

		if(nodelist->firstNode != NULL){
			(nodelist->firstNode)->previous = tempSender;////////////////////
		}
		
		nodelist->firstNode = tempSender;

		nodelist->numOfNodes = nodelist->numOfNodes + 1;
	}

	//printf("sender: %s\n",tempSender->name );

	tempReceiver = searchNodeList(receiverStr, nodelist);

	if(tempReceiver == NULL){  //paraleipths
		tempReceiver = nodeInit(receiverStr);
		tempReceiver->next = nodelist->firstNode;

		if(nodelist->firstNode != NULL){
			(nodelist->firstNode)->previous = tempReceiver;/////////////////
		}
		
		nodelist->firstNode = tempReceiver;

		nodelist->numOfNodes = nodelist->numOfNodes + 1;
	}

	//printf("Receiver: %s\n",tempReceiver->name );
//#########################//


//###########################//
	//prosthetw stis eiserxomenes akmes tou receiver mia akoma
	struct incomingEdge *inTemp;
	inTemp = searchIncomingEdgeList(senderStr,tempReceiver);
	if(inTemp == NULL){  //an den brei thn eiserxomenh apo ton senderstr thn ftiaxnei
		inTemp = inEdgeInit();
		inTemp->from = tempSender;

		inTemp->next = tempReceiver->inEdges;

		if(tempReceiver->inEdges != NULL){
			(tempReceiver->inEdges)->previous = inTemp;/////////////
		}
		
		tempReceiver->inEdges = inTemp;
		tempReceiver->numOfInEdges = tempReceiver->numOfInEdges + 1;
		//printf("tempReceiver->numOfInEdges: %d\n",tempReceiver->numOfInEdges );
		//printf("den to vrhka. to eftiaksa\n");
	}

	struct weights *tempWeightIn;  //prosthetw to varos sthn akmh
	tempWeightIn = weightInit();
	tempWeightIn->weight = atoi(weight);
	tempWeightIn->next = inTemp->weightsStart;

	if(inTemp->weightsStart != NULL){
		(inTemp->weightsStart)->previous = tempWeightIn;/////////
	}

	inTemp->weightsStart = tempWeightIn;
	inTemp->numOfWeights = inTemp->numOfWeights + 1;
//###########################//
	

//##########################//
	//prosthetw stis ekserxomenes akmes tou sender mia akoma
	struct outcomingEdge *outTemp;
	outTemp = searchoutcomingEdgeList(receiverStr,tempSender);
	if(outTemp == NULL){  //an den brei thn ekserxomenh apo ton receiverstr thn ftiaxnei
		outTemp = outEdgeInit();
		outTemp->to = tempReceiver;
		//outTemp->numOfWeights = outTemp->numOfWeights + 1;
		outTemp->next = tempSender->outEdges;

		if(tempSender->outEdges != NULL){
			(tempSender->outEdges)->previous = outTemp;////////////////
		}
		
		tempSender->outEdges = outTemp;
		tempSender->numOfOutEdges = tempSender->numOfOutEdges + 1;
	}

	struct weights *tempWeightOut;  //prosthetw to varos sthn akmh
	tempWeightOut = weightInit();
	tempWeightOut->weight = atoi(weight);
	tempWeightOut->next = outTemp->weightsStart;

	if(outTemp->weightsStart != NULL){
		(outTemp->weightsStart)->previous = tempWeightOut;//////////////
	}

	outTemp->weightsStart = tempWeightOut;
	outTemp->numOfWeights = outTemp->numOfWeights + 1;
	//printf("outTemp->numOfWeights %d\n", outTemp->numOfWeights);
//#######################//

	//tempSender->numOfOutEdges = tempSender->numOfOutEdges + 1;
	if(thereIsOutput != 10){                                  //****to exw kanei auto gia na krivontai ta munhmata apo to input arxeio
		printf("Inserted |%s|->%s->|%s|\n",senderStr,weight,receiverStr );  //exw vgalei ena \n
	}
	

	
}



void deleteNode(char *id, struct nodeList *nodelist, int thereIsOutput, FILE *fdOutput){
	struct node *tempNode;
	struct incomingEdge *tempIn;
	struct outcomingEdge *tempOut;
	struct weights *tempWeightIn;
	struct weights *tempWeightOut;

	//tempNode = searchNodeList(id, nodelist);
	//tempNodePrv = searchNodeListForPreviousOfId(id, nodelist);
	tempNode = searchNodeList(id,nodelist);
	//tempNode = (tempNodePrv->next);

	if(tempNode == NULL){  // h sunarthsh epistrefei NULL sto tempNodePrv ennowntas oti o komvos p psaxnoume den uparxei
		
		printf("- Node |%s| does not exist - abort-d;\n\n", id);

	}
	else{
		////////asxoloumai me indEdges
		tempIn = tempNode->inEdges; //ksekinaw me tis incoming akmes tou komvou
		while(tempIn!= NULL){

			while(tempIn->weightsStart != NULL){ //diagrafw ola ta varh tou tempIn
				tempWeightIn = tempIn->weightsStart;
				tempIn->weightsStart = tempWeightIn->next;
				free(tempWeightIn);
			}
			//printf("tempNode->name1 %s\n", tempNode->name);
			//printf("tempIn->from: %s\n",tempIn->from->name );
			

			tempOut = searchoutcomingEdgeList(id, tempIn->from); //paw ston sender na diagrapsw kai ekei thn outcoming akmh tou


			//if((tempIn->from)->numOfOutEdges > 1){
			while(tempOut->weightsStart != NULL){ //exw diagrapsei ola ta varh tou tempout tou komvou p m stelnei

				tempWeightOut = tempOut->weightsStart;
				tempOut->weightsStart = tempWeightOut->next;
				free(tempWeightOut);

			}

			if(tempOut->previous == NULL){ // einai o prwtos komvos

				(tempIn->from)->outEdges = tempOut->next;

				if( ((tempIn->from)->numOfOutEdges > 1) ){    // && (tempOut->next != NULL)
					(tempOut->next)->previous = NULL;
				}

				free(tempOut); //diagrafw ton kombo akmhs apo ton sender

			}
			else{ 

				(tempOut->previous)->next = tempOut->next;

				if(tempOut->next != NULL){
					(tempOut->next)->previous = tempOut->previous;
				}

				free(tempOut); //diagrafw ton kombo akmhs apo ton sender

			}


			(tempIn->from)->numOfOutEdges = (tempIn->from)->numOfOutEdges - 1;

			tempNode->inEdges = tempIn->next;
			free(tempIn); //diagrafw ton komvo akmhs apo ton receiver
			tempIn = tempNode->inEdges;
		}


		///////asxoloumai me outEdges
		tempOut = tempNode->outEdges;
		//printf("tempNode->name2 %s\n", tempNode->name);
		while(tempOut != NULL){

			while(tempOut->weightsStart != NULL){ //diagrafw ola ta varh tou tempOut
				tempWeightOut = tempOut->weightsStart;
				tempOut->weightsStart = tempWeightOut->next;
				free(tempWeightOut);
			}

			//printf("tempOut->to: %s\n",tempOut->to->name );
			//tempInPrv = searchIncomingEdgeListForDelete(id, tempOut->to); //paw ston receiver na diagrapsw kai ekei thn incoming akmh tou
			tempIn = searchIncomingEdgeList(id, tempOut->to);

			while(tempIn->weightsStart != NULL){ //exw diagrapsei ola ta varh tou tempin tou komvou p t stelnei

					tempWeightIn = tempIn->weightsStart;
					tempIn->weightsStart = tempWeightIn->next;
					free(tempWeightIn);

				}

			if(tempIn->previous == NULL){
				
				(tempOut->to)->inEdges = tempIn->next;

				if( ((tempOut->to)->numOfInEdges > 1) ){    // && (tempIn->next != NULL)
					(tempIn->next)->previous = NULL;
				}

				free(tempIn); //diagrafw ton kombo akmhs apo ton sender

			}
			else{
				
				(tempIn->previous->next) = tempIn->next;

				if(tempIn->next != NULL){
					(tempIn->next)->previous = tempIn->previous;
				}

				free(tempIn); //diagrafw ton kombo akmhs apo ton receiver

			}

			(tempOut->to)->numOfInEdges = (tempOut->to)->numOfInEdges - 1;

			tempNode->outEdges = tempOut->next;
			free(tempOut); //diagrafw ton komvo akmhs apo ton receiver
			tempOut = tempNode->outEdges;
		}

		
		if(tempNode->previous == NULL){
			nodelist->firstNode = tempNode->next;
			(tempNode->next)->previous = NULL;  //an einai o prwtos komvos kanw to previous null
		}
		else{
			(tempNode->previous)->next = tempNode->next;
			(tempNode->next)->previous = (tempNode->previous);
		}
		nodelist->numOfNodes = nodelist->numOfNodes - 1;
		

		printf("Deleted |%s|\n",id );
		
		
		free(tempNode->name);
		free(tempNode);
	}
}


void deleteEdge(char *sender, char *receiver, char *weightStr, struct nodeList *nodelist, int thereIsOutput, FILE *fdOutput){
	struct node *tempSender;
	struct node *tempReceiver;
	struct outcomingEdge *tempOutEdge;
	struct incomingEdge *tempInEdge;
	struct weights *weightIn;
	struct weights *weightOut;


	tempSender = searchNodeList(sender, nodelist);
	if(tempSender == NULL){

		printf("- |%s| does not exist - abort-l;\n",sender);

		return;
	}
	else{

	}

	tempReceiver = searchNodeList(receiver, nodelist);
	if(tempReceiver == NULL){

		printf("- |%s| does not exist - abort-l;\n",receiver);

		return;
	}

	//tempOutEdgePrv = searchoutcomingEdgeListForDelete(receiver, tempSender);
	tempOutEdge = searchoutcomingEdgeList(receiver, tempSender);
	if(tempOutEdge == NULL){

		printf("- |%s|->%s->|%s| does not exist - abort-l;\n",sender,weightStr,receiver);
		
		return;
	}

	//tempInEdgePrv = searchIncomingEdgeListForDelete(sender, tempReceiver);
	tempInEdge = searchIncomingEdgeList(sender, tempReceiver);
	if(tempInEdge == NULL){
		perror("kati exeis kanei lathos ston kwdika eisagwghs akmhs");
	}


	weightIn = searchWeightIn(weightStr,tempInEdge);
	if(weightIn == NULL){

		printf("- |%s|->%s->|%s| does not exist - abort-l;\n",sender,weightStr,receiver);

		return;
	}



	weightOut = searchWeightOut(weightStr,tempOutEdge);
	if(weightOut == NULL){
		perror("error");
	}

	printf("Del-vertex |%s|->%s->|%s|\n",sender,weightStr,receiver);


	if(tempInEdge->numOfWeights == 1){  //diagrafw to edge apo ton receiver
		
		if(tempInEdge->previous == NULL){ //diagrafw ton prwto komvo sthn lista inEdges

			tempReceiver->inEdges = tempInEdge->next;

			if(tempReceiver->numOfInEdges > 1){ //an exoume panw apo 1 komvous sth lista vazoume ston deikth previous tou deuterou komvou na deixnei null
				(tempInEdge->next)->previous = NULL;
			}
			
		}
		else{
			(tempInEdge->previous)->next = tempInEdge->next;

			if( tempInEdge->next != NULL){
				(tempInEdge->next)->previous = tempInEdge->previous;
			}
			
		}
		
		free(tempInEdge->weightsStart);
		free(tempInEdge);

		tempReceiver->numOfInEdges = tempReceiver->numOfInEdges - 1;
	}
	else{
		if(weightIn->previous == NULL){
			tempInEdge->weightsStart = weightIn->next;
			(weightIn->next)->previous = NULL;
		}
		else{

			(weightIn->previous)->next = weightIn->next;
			if(weightIn->next != NULL){
				(weightIn->next)->previous = weightIn->previous;
			}
		}

		free(weightIn);

		tempInEdge->numOfWeights = tempInEdge->numOfWeights - 1;

	}



	if(tempOutEdge->numOfWeights == 1){  //diagrafw to edge apo ton receiver
		
		if(tempOutEdge->previous == NULL){ //diagrafw ton prwto komvo sthn lista inEdges

			tempSender->outEdges = tempOutEdge->next;

			if(tempSender->numOfOutEdges > 1){ //an exoume panw apo 1 komvous sth lista vazoume ston deikth previous tou deuterou komvou na deixnei null
				(tempOutEdge->next)->previous = NULL;
			}
			
		}
		else{
			(tempOutEdge->previous)->next = tempOutEdge->next;

			if( tempOutEdge->next != NULL){
				(tempOutEdge->next)->previous = tempOutEdge->previous;
			}
			
		}
		
		free(tempOutEdge->weightsStart);
		free(tempOutEdge);

		tempSender->numOfOutEdges = tempSender->numOfOutEdges - 1;
	}
	else{
		if(weightOut->previous == NULL){
			tempOutEdge->weightsStart = weightOut->next;
			(weightOut->next)->previous = NULL;
		}
		else{

			(weightOut->previous)->next = weightOut->next;
			if(weightOut->next != NULL){
				(weightOut->next)->previous = weightOut->previous;
			}
		}

		free(weightOut);

		tempOutEdge->numOfWeights = tempOutEdge->numOfWeights - 1;

	}

}

void modifyEdge(char *sender, char *receiver, char *weightStr, char *newWeight, struct nodeList *nodelist, int thereIsOutput, FILE *fdOutput){
	struct node *tempSender;
	struct node *tempReceiver;
	struct outcomingEdge *tempOutEdge;
	struct incomingEdge *tempInEdge;
	struct weights *weightIn;
	struct weights *weightOut;


	tempSender = searchNodeList(sender, nodelist);
	if(tempSender == NULL){

		printf("- |%s| does not exist - abort-m;\n",sender);

		return;
	}


	tempReceiver = searchNodeList(receiver, nodelist);
	if(tempReceiver == NULL){

		printf("|%s| does not exist - abort-m;\n",receiver);

		return;
	}

	//tempOutEdgePrv = searchoutcomingEdgeListForDelete(receiver, tempSender);
	tempOutEdge = searchoutcomingEdgeList(receiver, tempSender);
	if(tempOutEdge == NULL){
		
		printf("|%s|->%s->|%s| does not exist - abort-m;\n",sender,weightStr,receiver);
		
		return;
	}

	//tempInEdgePrv = searchIncomingEdgeListForDelete(sender, tempReceiver);
	tempInEdge = searchIncomingEdgeList(sender, tempReceiver);
	if(tempInEdge == NULL){
		perror("kati exeis kanei lathos ston kwdika eisagwghs akmhs");
	}


	weightIn = searchWeightIn(weightStr,tempInEdge);
	if(weightIn == NULL){
		

		printf("|%s|->%s->|%s| does not exist - abort-m;\n",sender,weightStr,receiver);
		
		return;
	}



	weightOut = searchWeightOut(weightStr,tempOutEdge);
	if(weightOut == NULL){
		printf("|%s|->%s->|%s| does not exist - abort-m;\n",sender,weightStr,receiver);
		
		return;
	}



	printf("Mod-vertex |%s|->%s->|%s|\n",sender,newWeight,receiver);
	

	weightIn->weight = atoi(newWeight);
	weightOut->weight = atoi(newWeight);

}

void receivingEdges(char *receiver, struct nodeList *nodelist, int thereIsOutput, FILE *fdOutput){
	struct node *tempReceiver;
	struct incomingEdge *temp;
	struct weights *tempWeight;

	tempReceiver = searchNodeList(receiver, nodelist);
	if(tempReceiver == NULL){
		

		printf("|%s| does not exist - abort-r;\n",receiver );
		
		return;
	}

	temp = tempReceiver->inEdges;

	if(tempReceiver->numOfInEdges == 0){  //an den uparxei akmh
		

		printf("No-rec-edges |%s|\n",tempReceiver->name );
		
		return;
	}

	printf("rec-edges \n");

	while(temp != NULL){
		tempWeight = temp->weightsStart;
		// printf("from %s:\n", temp->from->name);

		//printf("	 weight:");
		while(tempWeight != NULL){

			printf("\t\t|%s|->%d->|%s|\n", temp->from->name, tempWeight->weight, receiver);

			tempWeight = tempWeight->next;
		}

		temp = temp->next;
	}

}


void circleFind(struct node *startingNode, char *StartingNodeStr, int thereIsOutput, FILE *fdOutput){
	struct node *temp;

// 	if(strcmp(StartingNodeStr, startingNode->name) == 0){
// 		printf("%s\n", startingNode->name);
// 	}
// 	else{
// 	printf("%s\n",startingNode->outEdges->to->name );
// 		circleFind(startingNode->outEdges->to, StartingNodeStr);
// 		circleFind(((startingNode->outEdges)->next)->to, StartingNodeStr);
// 	}

	// temp = startingNode;
	// printf("%s\n", startingNode->name);
	
	// while(temp != NULL){
	// 	do{

	// 	}while(strcmp(StartingNodeStr, startingNode->name) != 0)
	// }

}


void exitPr(struct nodeList *nodelist){
	struct node *Node;
	struct node *tempNode;
	struct incomingEdge *InEdge;
	struct incomingEdge *tempInEdge;
	struct outcomingEdge *OutEdge;
	struct outcomingEdge *tempOutEdge;
	struct weights *weight;
	struct weights *tempWeight;


	Node = nodelist->firstNode;

	while(Node != NULL){

		InEdge = Node->inEdges;
		while(InEdge != NULL){

			weight = InEdge->weightsStart;
			while(weight != NULL){
				tempWeight = weight;
				weight = weight->next;
				free(tempWeight);
			}

			tempInEdge = InEdge;
			InEdge = InEdge->next;
			free(tempInEdge);
		}


		OutEdge = Node->outEdges;
		while(OutEdge != NULL){

			weight = OutEdge->weightsStart;
			while(weight != NULL){
				tempWeight = weight;
				weight = weight->next;
				free(tempWeight);
			}

			tempOutEdge = OutEdge;
			OutEdge = OutEdge->next;
			free(tempOutEdge);
		}

		tempNode = Node;
		Node = Node->next;
		free(tempNode->name);
		free(tempNode);
	}

	free(nodelist);
	
}