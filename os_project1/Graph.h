//typedef struct inEdge incomingEdge;
//typedef struct outEdge outcomingEdge;

typedef struct node{  //struct komvou
	char *name;
	int numOfInEdges;  //deixnei apo posous diaforetikous lamvanw, ta weights deixnoun poses akmes uparxoun metaksu duo komvwn
	int numOfOutEdges; //deixnei se posous diaforetikous stelnw
	struct incomingEdge *inEdges;
	struct outcomingEdge *outEdges;
	struct node *previous;
	struct node *next;
} node;



typedef struct nodeList{  //arxh tou komvou
	struct node *firstNode;
	int numOfNodes;  //prepei na to afxanw kathe fora p kanw insert
} nodeList;

typedef struct weights{
	int weight;
	struct weights *previous;
	struct weights *next;
}weights;

typedef struct incomingEdge{  //eiserxomenes akmes
	struct node *from;
	int numOfWeights;
	struct weights *weightsStart;
	struct incomingEdge *previous;
	struct incomingEdge *next;
} incomingEdge;

typedef struct outcomingEdge{ //ekserxomenes akmes
	struct node *to;
	int numOfWeights;
	struct weights *weightsStart;
	struct outcomingEdge *previous;
	struct outcomingEdge *next;
} outcomingEdge;

