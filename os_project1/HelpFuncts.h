struct node *nodeInit(char *id);
struct node *searchNodeList(char *id, struct nodeList *nodeList);
struct node *searchNodeListForPreviousOfId(char *id, struct nodeList *nodelist);
struct nodeList *nodeListInit(void);
incomingEdge *inEdgeInit(void);
outcomingEdge *outEdgeInit(void);
struct outcomingEdge *searchoutcomingEdgeList(char *id, struct node *Node);
//struct outcomingEdge *searchoutcomingEdgeListForDelete(char *receiver, struct node *Node);
//struct incomingEdge *searchIncomingEdgeListForDelete(char *sender, struct node *Node);
struct incomingEdge *searchIncomingEdgeList(char *id, struct node *Node);
struct weights *weightInit(void);
struct weights *searchWeightIn(char *weight, struct incomingEdge *tempInEdge);
struct weights *searchWeightOut(char *weight, struct outcomingEdge *tempOutEdge);

void printAllNodes(struct nodeList *nodelist);
void PrintAllIncomingEdges(struct node *Node);
void printBeforeExit( int thereIsOutput, FILE *fdOutput, struct nodeList *nodelist);