void insertNode(char *id, struct nodeList *nodelist, int thereIsOutput, FILE *fdOutput);
void insertEdge(char *sender,char *receiver, char *weight, struct nodeList *nodelist, int thereIsOutput, FILE *fdOutput);
void deleteNode(char *id, struct nodeList *nodelist, int thereIsOutput, FILE *fdOutput);
void deleteEdge(char *sender, char *receiver, char *weight, struct nodeList *nodelist, int thereIsOutput, FILE *fdOutput);
void modifyEdge(char *sender, char *receiver, char *weightStr, char *newWeight, struct nodeList *nodelist, int thereIsOutput, FILE *fdOutput);
void receivingEdges(char *receiver, struct nodeList *nodelist, int thereIsOutput, FILE *fdOutput);
void circleFind(struct node *startingNode, char *StartingNodeStr, int thereIsOutput, FILE *fdOutput);
void exitPr(struct nodeList *nodelist);