#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "Graph.h"


#define SIZE 5

struct nodeList *nodeListInit(void){
	struct nodeList *nodeListTemp;
	nodeListTemp = malloc(sizeof(struct nodeList));

	nodeListTemp->numOfNodes = 0;
	nodeListTemp->firstNode = NULL;
	return nodeListTemp;
}

struct node *nodeInit(char *id){

	node *tempNode;
	tempNode = malloc(sizeof(node));

	tempNode->name = malloc(strlen(id)*sizeof(char) +1);
	strncpy(tempNode->name,id,strlen(id));
	tempNode->name[strlen(id)] = '\0';    //tempNode->name[strlen(tempNode->name)] prepei na to ksanakoitaksw

	tempNode->numOfInEdges = 0;
	tempNode->numOfOutEdges = 0;

	tempNode->inEdges = NULL;     //isws prepei na to desmeuw apo thn init
	tempNode->outEdges = NULL;	  //isws prepei na to desmeuw apo thn init

	tempNode->next = NULL;
	tempNode->previous = NULL;

	//printf("[new node]: '%s'\n",tempNode->name );

	return tempNode;
}




struct node *searchNodeList(char *id, struct nodeList *nodelist){

	struct node *temp;
	temp = nodelist->firstNode;

	while(temp != NULL){
		if(strcmp(temp->name, id) == 0){
			return temp;
		}
		temp = temp->next;
	}

	return NULL;
}

struct node *searchNodeListForPreviousOfId(char *id, struct nodeList *nodelist){

	struct node *temp;
	struct node *tempPrv;

	temp = nodelist->firstNode;
	tempPrv = temp;

	while(temp != NULL){
		if(strcmp(temp->name, id) == 0){
			return tempPrv;
		}
		tempPrv = temp;
		temp = temp->next;
	}

	return NULL;
}


void printAllNodes(struct nodeList *nodelist){
	struct node *temp;
	temp = nodelist->firstNode;
	int count = 1;
	while(temp != NULL){

		printf("%d %s\n",count, temp->name);
		temp = temp->next;
		count++;
	}
}

//####################################//

incomingEdge *inEdgeInit(void){
	incomingEdge *tempEdge;
	tempEdge = malloc(sizeof(incomingEdge));

	tempEdge->from = NULL;

	tempEdge->numOfWeights = 0;

	tempEdge->weightsStart = NULL;
	
	tempEdge->previous = NULL;
	tempEdge->next = NULL;

	return tempEdge;
}





outcomingEdge *outEdgeInit(void){
	outcomingEdge *tempEdge;
	tempEdge = malloc(sizeof(outcomingEdge));

	tempEdge->to = NULL;
	
	tempEdge->numOfWeights = 0;

	tempEdge->weightsStart = NULL;

	tempEdge->previous = NULL;
	tempEdge->next = NULL;

	return tempEdge;
}





struct incomingEdge *searchIncomingEdgeList(char *id, struct node *Node){

	struct incomingEdge *temp;
	temp = Node->inEdges;
	//printf("psaxnw gia: %s\n", id);
	while(temp != NULL){

		if(strcmp((temp->from)->name, id) == 0){
			return temp;
		}
		temp = temp->next;
	}

	return NULL;
}



struct outcomingEdge *searchoutcomingEdgeList(char *id, struct node *Node){

	struct outcomingEdge *temp;
	temp = Node->outEdges;

	while(temp != NULL){
		if(strcmp((temp->to)->name, id) == 0){
			return temp;
		}
		temp = temp->next;
	}

	return NULL;
}





struct weights *weightInit(void){
	struct weights *temp;
	temp = malloc(sizeof(weights));
	temp->weight = 0;
	temp->previous = NULL;
	temp->next = NULL;
	return temp;
}


void addWeight(){

}



struct weights *searchWeightIn(char *weight, struct incomingEdge *tempInEdge){
	struct weights *tempWeight;
	//struct weights *tempWeightPrv;
	tempWeight = tempInEdge->weightsStart;
	//tempWeightPrv = tempWeight;

	while(tempWeight != NULL){
		if(tempWeight->weight == atoi(weight)){
			return tempWeight;
		}
		tempWeight = tempWeight->next;
	}
	return NULL;

}


struct weights *searchWeightOut(char *weight, struct outcomingEdge *tempOutEdge){
	struct weights *tempWeight;

	tempWeight = tempOutEdge->weightsStart;


	while(tempWeight != NULL){
		if(tempWeight->weight == atoi(weight)){
			return tempWeight;
		}
		tempWeight = tempWeight->next;
	}
	return NULL;
}

void PrintAllIncomingEdges(struct node *Node){
	struct incomingEdge *temp;
	if( Node == NULL){
		printf("node does not exists\n");
		return;
	}
	temp = Node->inEdges;
	//printf("psaxnw gia: %s\n", id);
	struct weights *tempw;
	printf("Node->name %s\n",Node->name );
	if(Node->inEdges != NULL){


		while(temp != NULL){
			printf("%d edges from: %s\n", temp->numOfWeights, temp->from->name );

			tempw = temp->weightsStart;
			while(tempw != NULL){
				printf("  weight: %d\n",tempw->weight );
				tempw = tempw->next;
			}

			
			temp = temp->next;
		}
	}
	else{
		printf("there are no edges from \n");
	}
}



void printBeforeExit( int thereIsOutput, FILE *fdOutput, struct nodeList *nodelist){
	struct node *tempNode;
	struct outcomingEdge *outEdge;
	struct weights *tempWeight;
	tempNode = nodelist->firstNode;
	//printf("tempNode %s\n", tempNode->name);


	while(tempNode != NULL){
		//printf("tempNode %s\n", tempNode->name);
		outEdge = tempNode->outEdges;

		if(thereIsOutput == 1){
			fprintf(fdOutput, "|%s|\n",tempNode->name );
		}
		else{
			printf("|%s|\n",tempNode->name );
		}

		while(outEdge != NULL){

			tempWeight = outEdge->weightsStart;
			while(tempWeight != NULL){
				if(thereIsOutput == 1){
					fprintf(fdOutput, "  -%d->|%s|\n",tempWeight->weight,outEdge->to->name);
				}
				else{
					printf("  -%d->|%s|\n",tempWeight->weight,outEdge->to->name);
				}
				tempWeight = tempWeight->next;
			}
			
			outEdge = outEdge->next;
		}
		if(thereIsOutput == 1){
			fprintf(fdOutput, "\n");
		}
		else{
			printf("\n");
		}
		tempNode = tempNode->next;
	}
	
}
//na elegxw an oi entoles den dinontai swsta

