#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "Graph.h"
#include "HelpFuncts.h"
#include "Functions.h"


char *inputString( size_t size){  //
//The size is extended by the input with the value of the provisional
    char *str;
    int ch;
    size_t len = 0;
    str = realloc(NULL, sizeof(char)*size);//size is start size
    if(!str)return str;
    while(EOF!=(ch=getc(stdin)) && ch != '\n'){
        str[len++]=ch;
        if(len==size){
            str = realloc(str, sizeof(char)*(size+=16));
            if(!str)return str;
        }
    }
    str[len++]='\0';

    return realloc(str, sizeof(char)*len);
}




int main(int argc, char *argv[]){
	char *inputFile, *outputFile;



	if(argc == 1 || argc == 3 || argc == 5){

		struct nodeList *List; //ftiaxnw thn kefalh ths listas
		List = nodeListInit();

		int thereIsInput = 0, thereIsOutput = 0;  //elegxei an exei dwthei inputfile, outputfile h kai ta duo
		//diavazw ta orismata tou cmd
		for (int i = 0; i < argc; i++)
		{
			if(strcmp(argv[i], "-i") == 0){
				thereIsInput = 1;
				inputFile = malloc((strlen(argv[i +1]) + 1 ));
				strcpy(inputFile, argv[i+1]);
				//printf("%s\n",inputFile );
			}
			if(strcmp(argv[i], "-o") == 0){
				thereIsOutput = 1;
				outputFile = malloc((strlen(argv[i +1]) + 1 ));
				strcpy(outputFile, argv[i+1]);
				//printf("%s\n",outputFile );
				
			}
		}
		

		FILE *fdOutput;  //opening output file
		if(thereIsOutput == 1){
			fdOutput = fopen(outputFile, "w");
			if (fdOutput == NULL){
				perror("error opening file for output\n");
			}
		}
		
		if(thereIsInput == 1){

			FILE *fdInput;
			fdInput = fopen(inputFile, "r"); //anoigw to arxeio
			if (fdInput == NULL){
				perror("error opening file for input\n");
				return -1;
			}

			char *line = NULL;
			size_t bytes = 0;
			char *token;
			int times;
			char *sender, *receiver, *weight;


			while (getline(&line, &bytes, fdInput) != -1){

				times = 0;

				

				line[strlen(line) - 1] = '\0';
				/////////////////////////////////////////////
				// printf("%s\n",token );
				token = strtok(line, " \t\n");
				while(token != NULL){

					if(times == 0){
						sender = malloc(strlen(token) * sizeof(char) + 1);
						strcpy(sender, token);
						*(sender+strlen(token)) = '\0';
						//insertNode(sender, List);
					}
					else if(times == 1){
						receiver = malloc(strlen(token) * sizeof(char) + 1);
						strcpy(receiver, token);
						*(receiver+strlen(token)) = '\0';
						//insertNode(receiver, List);
					}
					else{
						weight = malloc(strlen(token) * sizeof(char) + 1);
						strcpy(weight, token);
						*(weight+strlen(token)) = '\0';  //giati xreiazetai mono sto teleutaio kommati tou strtok na bazw \0
						//printf("%s\n",weight);
					}
					
					times++;
					token = strtok(NULL, " \t");

				}

				insertEdge(sender, receiver, weight, List, 10, fdOutput);
				free(sender);
				free(receiver);
				free(weight);
				free(token);
				//printf("\n");

				//////////////////////////////////////////////
				//////////////////////////////////////////////

				
				
				/////////////////////////////////////////////
			}

			fclose(fdInput);
			free(inputFile);
			free(line);
		}
		

		char *mychar;

	  	while(1){
			//printf("prompt- ");
			mychar = inputString(100); 
			// printf("\n");

			char **tokens;
			tokens = malloc(6*sizeof(char*));
			char *token;
			int i=0;
			
			token = strtok(mychar, " \t");
			while(token != NULL){
				tokens[i] = malloc(strlen(token)*sizeof(char)+2);
				
				strcpy(tokens[i], token);
				// *(tokens[i] + strlen(tokens[i])) = '\0';
				// printf("token %d : %s\n",i,tokens[i] );
				i++;
				token = strtok(NULL, " \t");
			}
			//printf("%d\n",i );
			
			
			if(strcmp(tokens[0], "i") == 0){
				if(i == 2){

					
					insertNode(tokens[1], List, thereIsOutput, fdOutput);
				}
				else{
					printf("give a node name to insert\n");
				}
				
				// insertNode(, List);
			}
			else if(strcmp(tokens[0], "n") == 0){
				if(i == 4 && atoi(tokens[3]) != 0){
					insertEdge(tokens[1],tokens[2], tokens[3], List, thereIsOutput, fdOutput);
				}
				else{
					printf("give the correct arguments to insert edge\n");
				}
				
			}
			else if(strcmp(tokens[0], "d") == 0){
				if(i == 2){

				
					deleteNode(tokens[1], List, thereIsOutput, fdOutput);
				}
				else{
					printf("give a node name to delete\n");
				}
				
			}
			else if(strcmp(tokens[0], "l") == 0){
				if(i == 4 && atoi(tokens[3]) != 0){
					deleteEdge(tokens[1], tokens[2], tokens[3], List, thereIsOutput, fdOutput);
				}
				else{
					printf("give the correct arguments to delete edge\n");
				}
				
			}
			else if(strcmp(tokens[0], "m") == 0){
				if(i == 5 && atoi(tokens[3]) != 0 && atoi(tokens[4]) != 0){
					modifyEdge(tokens[1], tokens[2], tokens[3], tokens[4], List, thereIsOutput, fdOutput);
				}
				else{
					printf("give the correct arguments to modify edge\n");
				}
				
			}
			else if(strcmp(tokens[0], "r") == 0){
				if(i == 2){
					receivingEdges(tokens[1], List, thereIsOutput, fdOutput);
				}
				else{
					printf("please give a node to explore\n");
				}
				
			}
			else if(strcmp(tokens[0], "c") == 0){


			}
			else if(strcmp(tokens[0], "e") == 0){
				printBeforeExit( thereIsOutput, fdOutput, List);
				exitPr(List);

				// if(thereIsOutput == 1){
				// 	fprintf(fdOutput, "-exiting program");
				// }
				// else{
				// 	printf("-exiting program \n");
				// }
				printf("exit program\n");
				//apodesmeuw deiktes arxeiwn kai onomata arxeiwn
				if(thereIsOutput == 1){
				fclose(fdOutput);
				free(outputFile);
				}

				free(token);
				for(int j=0; j<i; j++){
					free(tokens[j]);
				}
				free(mychar);
				free(tokens);

				return 0;
			}
			else{
				printf("[note]: please enter an accepted command (i,n,d,l,m,r,c,e)\n");
			}
			printf("\n");
			free(token);  //ta free uparxoun duo fores, mia sto exit kai mia edw gia na ginontai ola ta free, kai twn upoloipwn entolwn kai tou free
			for(int j=0; j<i; j++){
				free(tokens[j]);
			}
			free(mychar);
			free(tokens);

		}

	}
	else{
		printf("please enter the correct arguments\n");
		return -1;
	}

}






// char **tokens;
				// tokens = malloc(6*sizeof(char*));
				// int i=0;

				// token = strtok(line, " \t\n");
				// while(token != NULL){
				// 	tokens[i] = malloc(strlen(token)*sizeof(char)+2);
				
				// 	strcpy(tokens[i], token);
				// 	//printf("token: %s\n",token ); ///////////////////////*******kovei to teleutaio pshfio
				// //printf("token %d : %s\n",i,tokens[i] );
				// 	i++;
				// 	token = strtok(NULL, " \t\n");
				// }


				// if(strcmp(tokens[0], "i") == 0){
				// 	if(i == 2){

				// 		insertNode(tokens[1], List, thereIsOutput, fdOutput);
				// 	}
				// 	else{
				// 		printf("give a node name to insert\n");
				// 	}
				
				// // insertNode(, List);
				// }
				// else if(strcmp(tokens[0], "n") == 0){
				// 	if(i == 4 && atoi(tokens[3]) != 0){
				// 		insertEdge(tokens[1],tokens[2], tokens[3], List, thereIsOutput, fdOutput);
				// 	}
				// 	else{
				// 		printf("give the correct arguments to insert edge\n");
				// 	}
					
				// }
				// else if(strcmp(tokens[0], "d") == 0){
				// 	if(i == 2){


				// 		deleteNode(tokens[1], List, thereIsOutput, fdOutput);
				// 	}
				// 	else{
				// 		printf("give a node name to delete\n");
				// 	}
					
				// }
				// else if(strcmp(tokens[0], "l") == 0){
				// 	if(i == 4 && atoi(tokens[3]) != 0){
				// 		deleteEdge(tokens[1], tokens[2], tokens[3], List, thereIsOutput, fdOutput);
				// 	}
				// 	else{
				// 		printf("give the correct arguments to delete edge\n");
				// 	}
					
				// }
				// else if(strcmp(tokens[0], "m") == 0){
				// 	if(i == 5 && atoi(tokens[3]) != 0 && atoi(tokens[4]) != 0){
				// 		modifyEdge(tokens[1], tokens[2], tokens[3], tokens[4], List, thereIsOutput, fdOutput);
				// 	}
				// 	else{
				// 		printf("give the correct arguments to modify edge\n");
				// 	}
					
				// }
				// else if(strcmp(tokens[0], "r") == 0){
				// 	if(i == 2){
				// 		receivingEdges(tokens[1], List, thereIsOutput, fdOutput);
				// 	}
				// 	else{
				// 		printf("please give a node to explore\n");
				// 	}
					
				// }
				// else if(strcmp(tokens[0], "c") == 0){


				// }
				// else if(strcmp(tokens[0], "e") == 0){
				// 	printBeforeExit( thereIsOutput, fdOutput, List);
				// 	// printf("%s\n",List->firstNode->name );
				// 	exitPr(List);

				// 	if(thereIsOutput == 1){
				// 		fprintf(fdOutput, "-exiting program");
				// 	}
				// 	else{
				// 		printf("-exiting program \n");
				// 	}
					
				// 	//apodesmeuw deiktes arxeiwn kai onomata arxeiwn
				// 	if(thereIsOutput == 1){
				// 	fclose(fdOutput);
				// 	free(outputFile);
				// 	}

				// 	free(token);
				// 	for(int j=0; j<i; j++){
				// 		free(tokens[j]);
				// 	}
					
				// 	free(tokens);

				// 	return 0;
				// }
				// else{
				// 	if(thereIsOutput == 1){
				// 		// fprintf(fdOutput, "-exiting program");
				// 		fprintf(fdOutput,"[note]: please enter an accepted command (i,n,d,l,m,r,c,e)\n");
				// 	}
				// 	else{
				// 		// printf("-exiting program \n");
				// 		printf("[note]: please enter an accepted command (i,n,d,l,m,r,c,e)\n");
				// 	}
					
				// }

				// free(token);  //ta free uparxoun duo fores, mia sto exit kai mia edw gia na ginontai ola ta free, kai twn upoloipwn entolwn kai tou free
				// for(int j=0; j<i; j++){
				// 	free(tokens[j]);
				// }
				
				// free(tokens);