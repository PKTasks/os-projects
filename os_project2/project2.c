#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include  <fcntl.h>
#include  <ctype.h>
#include  <poll.h>
#include  <signal.h>
#include <sys/stat.h>
#include <math.h>
#include "LeafNode.h"

volatile sig_atomic_t totalSignalsReceived = 0;


void handler(){

   totalSignalsReceived++;
}

int main(int argc, char *argv[]){
	int height = -1;
	char *inputFile;
	char *pattern;
	int flagS = 0;
	
	if(argc == 7 || argc==8){
		for (int i = 0; i < argc; i++)
		{	
			
			if(strcmp(argv[i], "-h") == 0){
				
				height = atoi(argv[i+1]);
				if(height <1 || height > 5){
					printf("epitrepomeno upsos 1-5\n");
					return -1;
				}
				// printf("%d\n",height );
			}
			if(strcmp(argv[i], "-d") == 0){
				inputFile = malloc((strlen(argv[i +1]) + 1 ));
				strcpy(inputFile, argv[i+1]);
				// printf("%s\n",inputFile );
				
			}
			if(strcmp(argv[i], "-p") == 0){
				pattern = malloc((strlen(argv[i +1]) + 1 ));
				strcpy(pattern, argv[i+1]);
				// printf("%s\n",pattern );
				
			}
			if(strcmp(argv[i], "-s") == 0){
				flagS = 1;
			}
		}
	}
	else{
		printf("give the right arguments\n");
		return -1;

	}


	// ## ftiaxnw ta fifo
	struct stat st = {0};

	if (stat("./fifos/", &st) == -1) {
    	mkdir("./fifos/", 0700);
	}
	
	char *fifoRootRead;
	char *fifoRootWrite;
	fifoRootRead = malloc(strlen("./fifos/fifoRoot")*sizeof(char)+6);
	// fifoRootWrite = malloc(strlen("./fifos/fifoRoot")*sizeof(char)+8);

	strcpy(fifoRootRead,"./fifos/fifoRoot");
	strcat(fifoRootRead,"_");
	strcat(fifoRootRead,"Read");
	

	int fifo = mkfifo(fifoRootRead , 0666);
	if(fifo < 0){
		perror("mkfifo");
		return -1;
	}
	
	


	//##
	struct sigaction sa;
	sa.sa_handler = handler;
	sa.sa_flags = SA_RESTART;
	sigfillset(&sa.sa_mask);
	if (sigaction(SIGUSR2, &sa, NULL) == -1) {
        perror("sigaction");
        return -1;
    }

	//##

	//####
	int pid;
	// printf("root: my pid is %d\n", getpid());
	pid = fork();
	if(pid == 0){
			
		char remainingHeight[12];
		sprintf(remainingHeight,"%d",height);
		char flagSstr[2];
		sprintf(flagSstr,"%d",flagS);

		char startingSearcher[5];
		sprintf(startingSearcher,"%d",1);
		char endingSearcher[5];
		sprintf(endingSearcher,"%d",(int)pow(2,height));

		char rootpidStr[10];
		sprintf(rootpidStr,"%d",getppid());

		char totalSearchersStr[10];
		sprintf(totalSearchersStr,"%d",(int)pow(2,height));
		
		if(execlp("./SMNode","SMNode",remainingHeight,inputFile,flagSstr,pattern,remainingHeight,startingSearcher,endingSearcher,rootpidStr,fifoRootRead,totalSearchersStr,(char*)NULL) == -1){
			perror("exec");
		};
	}
	

	int fdPipeRead = open(fifoRootRead, O_RDONLY);

	//## diavazw apo to paidi
	MyRecord **rec;
	int bufSize = SIZE;
	rec = malloc(SIZE * sizeof(MyRecord*)); //apothhkeuw tis eggrafes se ena dunamiko pinaka
	int k = 0;

	int readM;
	MyRecord temprec;
	while((readM = read(fdPipeRead,&temprec,sizeof(MyRecord))) > 0){ //diavazw apo to pipe mexri na epistrepsei 0
		// printf("%d\n",k );
		if (k == bufSize){ //orizw ena arxiko megethos pinaka. an gemisei auto kanw realloc
			rec = realloc(rec, 2*bufSize* sizeof(MyRecord*));
			bufSize = 2*bufSize;
		}
		(*(rec+k)) =  malloc(1*sizeof(MyRecord));
		memmove((*(rec+k)),&temprec,sizeof(MyRecord));
		// 	printf("0 readM %d  ",readM );
		    	printf("%ld %s %s  %s %d %s %s %-9.2f\n", \
		(*(rec+k))->custid, (*(rec+k))->LastName, (*(rec+k))->FirstName, \
		(*(rec+k))->Street, (*(rec+k))->HouseID, (*(rec+k))->City, (*(rec+k))->postcode, \
		(*(rec+k))->amount);

		k++;

	}
	printf("\ntotal lines found with pattern: %d\n",k );
	//##

	waitpid(pid, NULL, 0);
	//####



	//## kleinei to fifo pipe p xrhsimopoiouse mexri twra
	close(fdPipeRead);
	remove(fifoRootRead);
	free(fifoRootRead);
	//##


	//## kalei thn sort
	int readFromSortP[2];
	int writeToSortP[2];

	if(pipe(readFromSortP) == -1){
		perror("pipe");
		exit(1);
	}
	if(pipe(writeToSortP) == -1){
		perror("pipe");
		exit(1);
	}
	int oldstdin =  dup(STDIN_FILENO);
	int oldstdout = dup(STDOUT_FILENO);

	close(STDIN_FILENO);
	close(STDOUT_FILENO);

	if(dup2(writeToSortP[0],STDIN_FILENO) == -1){
		perror("dup2");
		exit(1);
	}
	if(dup2(readFromSortP[1],STDOUT_FILENO) == -1){
		perror("dup2");
		exit(1);
	}


	int pidSort = fork();
	if(pidSort == 0){
		close(writeToSortP[0]);
		close(writeToSortP[1]);
		close(readFromSortP[0]);
		close(readFromSortP[1]);
		
		if(execlp("sort","sort","-g",(char*)NULL) == -1){
			perror("exec");
		}
	}
	else{
		close(STDIN_FILENO);
		close(STDOUT_FILENO);
		dup2(oldstdin,STDIN_FILENO);
		dup2(oldstdout,STDOUT_FILENO);
		close(writeToSortP[0]);
		close(readFromSortP[1]);

		int writen;
		char buffer[sizeof(MyRecord)];
		char custidStr[10];
	   	char housIDStr[10];
	   	char ammountStr[10];
	   	
	   	printf("\n");

		for(int i = 0; i < k; i++){
			// printf("%d\n",i );
			sprintf(custidStr,"%ld",(*(rec+i))->custid);
			sprintf(housIDStr,"%d",(*(rec+i))->HouseID);
			sprintf(ammountStr,"%-9.2f",(*(rec+i))->amount);
			// int totalLenght = strlen(custidStr)+strlen(housIDStr)+strlen(ammountStr)+strlen((*(rec+i))->LastName)+strlen((*(rec+i))->FirstName)+strlen((*(rec+i))->Street)+strlen((*(rec+i))->City)+strlen((*(rec+i))->postcode);

			snprintf(buffer,BUFFERSIZE,"%s %s %s %s %s %s %s %s\n",custidStr,(*(rec+i))->LastName, (*(rec+i))->FirstName, (*(rec+i))->Street, housIDStr, (*(rec+i))->City, (*(rec+i))->postcode, ammountStr);
			// printf("%s",buffer );
			if (writen = write(writeToSortP[1],buffer,strlen(buffer)) <= 0){
				perror("write");
			}

		}
		close(writeToSortP[1]);
		waitpid(pidSort,NULL,0);


		char input[sizeof(MyRecord)];
		char *out;
		out = malloc(k*sizeof(MyRecord)*sizeof(char)+1);
		int readS;
		int firstRead = 0;

		while(readS = read(readFromSortP[0],input,sizeof(MyRecord)) > 0){
			
			if(firstRead == 0){
				strcpy(out,input);
				firstRead = 1;
			}
			else{
				strcat(out,input);
			}
			
			
			// printf("!!%s\n",input );
		}

		close(readFromSortP[0]);
		// printf("%s\n",out );

		char *token;
		int i = 0;
		token = strtok(out,"\n");
		while(token != NULL){
			if(i < k){
				printf("%s\n",token );
				i++;
			}
			
			token = strtok(NULL,"\n");
		}
		printf("\ntotal lines returned sorted: %d\n",k );
		free(out);
	}
	
	//##





	printf("\ntotal SIGUSR2 received: %d\n",totalSignalsReceived );


	free(inputFile);
	free(pattern);


	for(int i = 0; i < k;i++){
		free((*(rec+i)));
	}
	free(rec);
}


