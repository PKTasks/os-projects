#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
char *fifoReadCreate(int i, char *pidStr, char *remainingHeight){
	char *SM1;
	char iStr[2];
	SM1 = malloc(strlen("./fifos/SMX_")*sizeof(char)+sizeof(int)+8);

	int fifo;
	sprintf(iStr,"%d",i);
	strcpy(SM1,"./fifos/SM");
	strcat(SM1,remainingHeight );
	strcat(SM1,"_" );
	strcat(SM1,pidStr);
	strcat(SM1,"_");
	strcat(SM1,"Read");
	strcat(SM1,iStr);

	fifo = mkfifo(SM1 , 0666);
	if(fifo < 0){
		perror("mkfifo");
		exit(-1);
	}
	return SM1;
}



void fifoReadRemove(char **read){
	remove(read[0]);
	remove(read[1]);
	free(read[0]);
	free(read[1]);
}

char *fifoWriteCreate(int i, char *pidStr, char *remainingHeight){
	char *SM1;
	char iStr[2];
	SM1 = malloc(strlen("./fifos/SMX_")*sizeof(char)+sizeof(int)+8);

	sprintf(iStr,"%d",i);
	strcpy(SM1,"./fifos/SM");
	strcat(SM1,remainingHeight );
	strcat(SM1,"_" );
	strcat(SM1,pidStr);
	strcat(SM1,"_");
	strcat(SM1,"write");
	strcat(SM1,iStr);

	int fifo = mkfifo(SM1 , 0666);
	if(fifo < 0){
		perror("mkfifo");
		exit(-1);
	}
	return SM1;
}



void fifoWriteRemove(char **write){
	remove(write[0]);
	remove(write[1]);
	free(write[0]);
	free(write[1]);
}