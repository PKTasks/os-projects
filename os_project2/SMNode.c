#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include  <fcntl.h>
#include  <ctype.h>
#include  <poll.h>
#include  <signal.h>
#include <sys/stat.h>
#include <math.h>

// #include "SMNode.h"
#include "SMFunctions.h"
#include "LeafNode.h"

#define BUFFSIZE 120


//~~~~~~~ argv[2] inputFile
//~~~~~~~ argv[3] flagS
//~~~~~~~ argv[4] pattern
//~~~~~~~ argv[5] height
//~~~~~~~ argv[6] starting searcher
//~~~~~~~ argv[7] ending searcher
//~~~~~~~ argv[8] root's pid
//~~~~~~~ argv[9] pipe name
//~~~~~~~ argv[10] totalSearchers


void handler(){
   signal(SIGTERM,handler);
   printf("child: I have received a SIGINT from parent\n");
}

int main(int argc, char *argv[]){
	// printf("SM: my pid is %d. my parent id is [%d]. height %s\n",getpid(), getppid(),argv[1] );
	
	
	int remainingHeight = -1;
	remainingHeight = atoi(argv[1]);
	int height = atoi(argv[5]);


	//## kanw open to pipe p epikoinwnw me ton patera
	int fdPipefather;
	fdPipefather = open(argv[9],O_WRONLY);
	//##


	//## dhmiourgo ta pipes p tha exw gia epikoinwnia me ta paidia ths diergasias
	char pidStr[12];
	sprintf(pidStr,"%d",getpid());

	char **SMRead;
	char **SMWrite;
	SMRead = malloc(2*sizeof(char*));
	// SMWrite = malloc(2*sizeof(char*));
	int fdReadChild[2];
	int fdWrite[2];

	
	for(int i = 0; i< 2;i++){
		SMRead[i] = fifoReadCreate(i,pidStr, argv[1]);
		
		// fdReadChild[i] = open(SMRead[i], O_RDONLY | O_NONBLOCK ); //kanw open to pipe p diavazw apo ta paidia
	}
	//##	

	
	//##dhmiourgo ta paidia
	remainingHeight = remainingHeight -1;
	int pid[2];
	char start[5];
	char middle[5];
	char end[5];

	for(int i = 0; i < 2; i++){
		pid[i] = fork();

		if(pid[i] == 0){

			// printf("my pid is %d. my parent id is %d\n", getpid(), getppid());
			char remainingHeightStr[12];
			sprintf(remainingHeightStr,"%d",remainingHeight);
			
			
			if(remainingHeight != 0){ // anadromika dhmiourgo eite splitter-merger eite searcher
				if(i == 0){
					sprintf(start,"%d",atoi(argv[6]));
					sprintf(middle,"%d",(atoi(argv[7])- atoi(argv[6]))/2 + atoi(argv[6]));
					

					if(execlp("./SMNode","SMNode",remainingHeightStr,argv[2],argv[3],argv[4],argv[5],start,middle,argv[8],SMRead[0],argv[10],(char*)NULL) == -1){
						perror("exec");
					}
				}
				else{
					
					sprintf(middle,"%d",(atoi(argv[7])- atoi(argv[6]))/2 + atoi(argv[6]) +1);
					sprintf(end,"%d",atoi(argv[7]));

					if(execlp("./SMNode","SMNode",remainingHeightStr,argv[2],argv[3],argv[4],argv[5],middle,end,argv[8],SMRead[1],argv[10],(char*)NULL) == -1){
						perror("exec");
					}
				}

			}
			else{
				if(i == 0){
					if(execlp("./LeafNode","LeafNode",remainingHeightStr,argv[2],argv[3],argv[4],argv[5],argv[6],argv[8],SMRead[0],argv[10],(char*)NULL) == -1){
						perror("exec");
					}
				}
				else{
					if(execlp("./LeafNode","LeafNode",remainingHeightStr,argv[2],argv[3],argv[4],argv[5],argv[7],argv[8],SMRead[1],argv[10],(char*)NULL) == -1){
						perror("exec");
					}
				}
			}
			
		}
	}
	//##

	for(int i = 0; i< 2;i++){
		fdReadChild[i] = open(SMRead[i], O_RDONLY);
	}

	//##
	// char buff[BUFFSIZE];
	// memset(buff,0,BUFFSIZE);
	// int readM;
	
	// while((readM = read(fdReadChild[1],buff,BUFFSIZE)) > 0){
	// 	printf("%d\n",readM );
	// 	buff[readM+1] = '\0';
	
	// 	printf("!!!! i am reading: %s\n",buff );
	// }
	
	//##

	//## diavazw apo ta duo paidia
	MyRecord **rec;
	rec = malloc(SIZE * sizeof(MyRecord*));
	int bufSize = SIZE;
	int k = 0;

	int readM;
	MyRecord temprec;
	

	for(int i = 0; i< 2; i++){ 
		while((readM = read(fdReadChild[i],&temprec,sizeof(MyRecord))) > 0){ //diavazw apo to pipe mexri na epistrepsei 0

			if (k == bufSize){
				rec = realloc(rec, 2*bufSize* sizeof(MyRecord*));
				bufSize = 2*bufSize;
			}
			(*(rec+k)) =  malloc(1*sizeof(MyRecord));
			memmove((*(rec+k)),&temprec,sizeof(MyRecord));
			
			k++;
		}
	}

	//##


	//## perimenw na teleiwsoun ola ta paidia
	for(int i = 0; i < 2; i++){
		waitpid(pid[i], NULL, 0);
	}
	//##

	//## grafw sto pipe tou patera
	int writen = 0;
	
	for(int i = 0; i < k;i++){
		if (writen = write(fdPipefather,(*(rec+i)),sizeof(MyRecord)) <= 0){
			perror("write");
		}
	}
	//##
	
	//##
	close(fdPipefather);
	close(fdReadChild[0]);
	close(fdReadChild[1]);
	fifoReadRemove(SMRead);
	//##

	for(int i = 0; i < k;i++){
		free((*(rec+i)));
	}
	free(rec);
	
	// fifoWriteRemove(SMWrite);

	free(SMRead);
	exit(0);

}
