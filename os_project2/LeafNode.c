#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include  <fcntl.h>
#include  <ctype.h>
#include  <poll.h>
#include  <signal.h>
#include <sys/stat.h>
#include <math.h>

#include "LeafNode.h"



int substring(char *s1, char *s2) { 
    int M = strlen(s1); 
    int N = strlen(s2); 
  
    /* A loop to slide pat[] one by one */
    for (int i = 0; i <= N - M; i++) { 
        int j; 
  
        /* For current index i, check for pattern match */
        for (j = 0; j < M; j++) 
            if (s2[i + j] != s1[j]) 
                break; 
  
        if (j == M) 
            return i; 
    } 
  
    return -1; 
} 




int main(int argc, char *argv[]){
	// printf("Leaf: my pid is %d. my parent id is [%d]. height %s\n",getpid(), getppid(),argv[1] );
	

	char *inputFile;
	char *pattern;
	int flagS, height;
	int searcherNumber = 0;
	int totalSearchers = 0;
	for(int j = 1; j <= argc; j++){
		if(j == 1){

		}
		else if(j == 2){
			inputFile = malloc((strlen(argv[j]) + 1 ));
			strcpy(inputFile, argv[j]);
			// printf("%s\n",inputFile );
		}
		else if(j == 3){
			flagS = atoi(argv[j]);
			// printf("flagS %d\n",flagS );
		}
		else if(j == 4){
			pattern = malloc((strlen(argv[j]) + 1 ));
			strcpy(pattern, argv[j]);
			// printf("%s\n",pattern );
		}
		else if(j == 5){
			height = atoi(argv[j]);
			// printf("height %d\n", height);
		}
		else if(j == 6){
			searcherNumber = atoi(argv[6]);
			// printf("searchernumber %d\n", searcherNumber);
		}
		else if(j == 9){
			totalSearchers = atoi(argv[9]);
			// printf("totalSearchers %d\n", totalSearchers);
		}
	}


	//## anoigei to arxeio
	FILE *fpb;
	MyRecord rec;
   	long lSize;
   	int numOfrecords, i;

   	fpb = fopen (argv[2],"rb");
   	if (fpb==NULL) {
      	printf("Cannot open binary file\n");
      	exit(1);
   	}
   	//##



   	//## open pipe
   	int fdFather = open(argv[8], O_WRONLY);

   	//## check number of records
   	fseek (fpb , 0 , SEEK_END);
   	lSize = ftell(fpb);
   	rewind (fpb);
   	numOfrecords = (int) lSize/sizeof(rec);
   	//##


   	// printf("Records found in file %d \n", numOfrecords);
   	int count = 0;
   	int recordsToRead = 0;

   	//## upologizw poses eggrafes tha diavasw me vash to flag s
   	if(flagS == 1){
   		int sum = 0;
   		for(int n = 1; n <= (int)pow(2,height);n++){
			sum = sum + n;
		}	

		
		for(int j = 1; j < searcherNumber; j++){  //vriskw poses egrafes exoun diabasei oi prohgoumenoi
			count = count + (j*numOfrecords)/sum;
			// printf("%d %d\n",j, count );
		}
		recordsToRead = (searcherNumber*numOfrecords)/sum;

		int sparelines = 0;
		if(searcherNumber == totalSearchers){ //upologizw tuxon grammes p perissepsan meta to moirasma
			sparelines = numOfrecords - (count + recordsToRead);
			// printf("%d\n",sparelines );
			recordsToRead = recordsToRead + sparelines;
		}
   	} 
   	else{
   		for(int j = 1; j < searcherNumber; j++){  //vriskw poses egrafes exoun diabasei oi prohgoumenoi
			count = count + numOfrecords/(int)pow(2,height);
		}

		recordsToRead = numOfrecords/(int)pow(2,height);

   	}
   	//##


   	fseek (fpb , (count+1)*sizeof(rec) , SEEK_SET); // vazw to fd sthn epomenh thesh ths ths teleutaias diabasmenhs grammhs
   	lSize = ftell(fpb);
   	numOfrecords = (int) lSize/sizeof(rec);
   	// printf("records have read %d\n", numOfrecords-1);
   	// printf("records to read %d\n", recordsToRead);


   	char custidStr[10];
   	char housIDStr[10];
   	char ammountStr[10];
   


   	for (i=0; i<recordsToRead ; i++) {
      	fread(&rec, sizeof(rec), 1, fpb);
      	

		
		sprintf(custidStr,"%ld",rec.custid);
		sprintf(housIDStr,"%d",rec.HouseID);
		sprintf(ammountStr,"%-9.2f",rec.amount);
		//##elegxw an to pattern uparxei mesa sto record
		if((substring(pattern,custidStr) != -1) || (substring(pattern,rec.LastName) != -1) || (substring(pattern,rec.FirstName) != -1) || (substring(pattern,rec.Street) != -1) || (substring(pattern,housIDStr) != -1) || (substring(pattern,rec.City) != -1) || (substring(pattern,rec.postcode) != -1) || (substring(pattern,ammountStr) != -1)){
			// printf("!!!!!!!!!!!!!!!!!!!!!!!!%d\n", searcherNumber);
						
			// 	    	printf("%ld %s %s  %s %d %s %s %-9.2f\n", rec.custid, rec.LastName, rec.FirstName, \
			// rec.Street, rec.HouseID, rec.City, rec.postcode, \
			// rec.amount);


			//##
			int writen = 0;
			// write(fdFather,&rec,sizeof(rec));
			if (writen = write(fdFather,&rec,sizeof(MyRecord)) <= 0){
				perror("write");
			}
			//##
		}
		
   	}

   	int rootsId;
   	rootsId = atoi(argv[7]);
   	kill(rootsId,SIGUSR2);

   	close(fdFather);
   	fclose (fpb);
   	free(inputFile);
   	free(pattern);
   	exit(0);
}
