char *fifoReadCreate(int i, char *pidStr, char *remainingHeight);
char *fifoWriteCreate(int i, char *pidStr, char *remainingHeight);
void fifoReadRemove(char **read);
void fifoWriteRemove(char **write);